#!/usr/bin/env bash

set -euo pipefail

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
cd "$DIR" && source utils.sh && cd ".."

CWD=$(pwd)

# shellcheck disable=SC2059
function main() {
  printf -- "$F_BOLD# Resource-Packs packing #\n$F_RESET"
  iterate "data" pack_all
  printf -- "\n"
  "$DIR"/generate-readme.sh
  printf -- "$F_BOLD\nDONE.\n$F_RESET"
}

function pack_all() {
  local path=${1}
  version=$(filename "$path")
  iterate "$path" pack
}

function pack() {
  local path=${1}
  local name
  name=$(filename "$path")
  local zip_path="out/$version-$name.zip"

  local last_run
  [[ -f "$zip_path" ]] && last_run=$(modification_time "$zip_path") || last_run=0

  local last_change
  last_change=$(modification_time "$path")
  if [[ ${last_change} -lt ${last_run} ]]; then
    printf -- "Packing $F_BOLD%s:%s$F_RESET [UP-TO-DATE]\n" "$version" "$name"
    return 0
  fi

  cd "$path" || exit
  printf -- "Packing of $F_BOLD%s:%s$F_RESET\n$F_GRAY" "$version" "$name"
  rm "$CWD/$zip_path" || true
  zip -r "$CWD/$zip_path" ./*
  # shellcheck disable=SC2059
  printf -- "$F_RESET"
  cd "$CWD" || exit

  printf -- "Hash: %s\n\n" "$(sha1 "$zip_path")"
}

main
