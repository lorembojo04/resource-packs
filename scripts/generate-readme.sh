#!/usr/bin/env bash

set -euo pipefail

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
cd "$DIR" && source utils.sh && cd ".."

README_TMP="README.md.tmp"
README="README.md"

function generate_readme() {
  # Remove tmp if it already exists
  [[ -f ${README_TMP} ]] && rm ${README_TMP}

  # shellcheck disable=SC2059
  printf -- "$F_BOLD# Generating README #\n$F_RESET"
  append_file_header
  version=""
  iterate "out" add_rp_to_readme

  mv ${README_TMP} ${README}
}

function add_rp_to_readme() {
  local path=$1
  local file_name
  file_name=$(filename_strip_extension "$path")
  local curr_version=${file_name%%-*}
  local name=${file_name#*-}

  [[ "$curr_version" != "$version" ]] && append_version_header "$curr_version"
  append "| [$name]($path) | $(sha1 "$path") |"
  printf -- "Added $F_BOLD%s:%s$F_RESET\n" "$version" "$name"
}

function append_file_header() {
  append \
    "# RPGInventory Resource Packs" \
    "<!-- NOTE: This file generated with script all changes will be replaced. DO NOT change this. -->" \
    "" \
    "| RP version | MC version |" \
    "|------------|------------|" \
    "| v2         |  1.9-1.10  |" \
    "| v3         | 1.11-1.12  |" \
    "| v4         | 1.13-1.14  |" \
    "| v5         | 1.15+      |"
}

function append_version_header() {
  version=$1
  append \
    "" \
    "### $version" \
    "" \
    "| Name | Hash (SHA1) |" \
    "|------|-------------|"
}

function append() {
  for i in "$@"; do
    printf "%s\n" "$i" >>${README_TMP}
  done
}

generate_readme
