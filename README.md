# RPGInventory Resource Packs
<!-- NOTE: This file generated with script all changes will be replaced. DO NOT change this. -->

| RP version | MC version |
|------------|------------|
| v2         |  1.9-1.10  |
| v3         | 1.11-1.12  |
| v4         | 1.13-1.14  |
| v5         | 1.15+      |

### v2

| Name | Hash (SHA1) |
|------|-------------|
| [vanilla](out/v2-vanilla.zip) | d2543357e4b5f6535967b7f97cf14e9fc2da6da3 |

### v3

| Name | Hash (SHA1) |
|------|-------------|
| [vanilla-wooden](out/v3-vanilla-wooden.zip) | 2093b1d62db439166c190fb1b3b9cfac0072c890 |
| [vanilla](out/v3-vanilla.zip) | c9b9ab28395dab0ec2c618246c32d331fc4bf913 |

### v4

| Name | Hash (SHA1) |
|------|-------------|
| [vanilla](out/v4-vanilla.zip) | 4c365d9b8794a4b0ee64bb168d8b325a8b1c6f73 |

### v5

| Name | Hash (SHA1) |
|------|-------------|
| [vanilla](out/v5-vanilla.zip) | b6a85f957de8f57d976ccb0eea48bba4fb3d4fc8 |
